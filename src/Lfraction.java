import java.util.*;

/**
 * This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

    /**
     * Main method. Totally useless. Different tests. All tests should be in test class
     */
    public static void main(String[] param) {

        Lfraction lfac = new Lfraction(3,4);
        System.out.println(lfac.pow(-2));
        // TODO!!! Your debugging tests here
    }

    // TODO!!! instance variables here
    // Keeping it Long and not long removes ugly casting.
    private Long numerator;
    private Long denominator;

    /**
     * Constructor.
     * Reduces fraction.
     *
     * @param a numerator
     * @param b denominator > 0
     */
    public Lfraction(long a, long b) {
        if (b == 0) {
            throw new RuntimeException("Panic! Division with 0 is impossible.");
        }
        long numerator = a / gcd(a, b);
        long denominator = b / gcd(a, b);
        // requirement from forum: nimetaja (aka. denominator) on alati positiivne -> if fraction is < 0 => numerator gets the minus.
        if ( denominator < 0){
            this.denominator = -denominator;
            this.numerator = -numerator;
        } else {
            this.denominator = denominator;
            this.numerator = numerator;
        }

        // TODO!!!
    }

    private static long gcd(long a, long b) {
        //Idea from https://www.vogella.com/tutorials/JavaAlgorithmsEuclid/article.html
        if (b == 0) {
            return a;
        }
        return gcd(b, a % b);
    }

    /**
     * Public method to access the numerator field.
     *
     * @return numerator
     */
    public long getNumerator() {
        return numerator; // TODO!!!
    }

    /**
     * Public method to access the denominator field.
     *
     * @return denominator
     */
    public long getDenominator() {
        return denominator; // TODO!!!
    }

    /**
     * Conversion to string.
     *
     * @return string representation of the fraction
     */
    @Override
    public String toString() {
        String s = numerator + "/" + denominator;
        return s; // TODO!!!
    }

    /**
     * Equality test.
     *
     * @param m second fraction
     * @return true if fractions this and m are equal
     */
    @Override
    public boolean equals(Object m) {
        if (m instanceof Lfraction) {
            Lfraction frac = (Lfraction) m;
            return frac.getDenominator() == getDenominator() && frac.getNumerator() == getNumerator();
        }
        return false; // TODO!!!
    }

    /**
     * Hashcode has to be the same for equal fractions and in general, different
     * for different fractions.
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        // Help from https://www.baeldung.com/java-objects-hash-vs-objects-hashcode#3-objectshash
        return Objects.hash(this.denominator, this.numerator); // TODO!!!
    }

    /**
     * Sum of fractions.
     *
     * @param m the fraction to be added
     * @return this+m
     */
    public Lfraction plus(Lfraction m) {
        long denominator = getDenominator() * m.getDenominator();
        long nominator = getNumerator() * m.getDenominator() + m.getNumerator() * getDenominator();
        return new Lfraction(nominator, denominator); // TODO!!!
    }

    /**
     * Multiplication of fractions.
     *
     * @param m second factor
     * @return this*m
     */
    public Lfraction times(Lfraction m) {
        long nominator = getNumerator() * m.getNumerator();
        long denominator = getDenominator() * m.getDenominator();
        return new Lfraction(nominator, denominator); // TODO!!!
    }

    public Lfraction pow(int i){

        if (i == 0){
            return new Lfraction(1,1);
        }
        else if (i == 1){
            return new Lfraction(getNumerator(), getDenominator());
        }
        else if (i == -1){
            return this.inverse();
        }
        else if (i <= -2) {
            return this.pow(-i).inverse();
        }
        return this.times(this.pow(i-1));
    }

    /**
     * Inverse of the fraction. n/d becomes d/n.
     *
     * @return inverse of this fraction: 1/this
     */
    public Lfraction inverse() {
        if (getNumerator() == 0){
            throw new RuntimeException("Panic! Turning numerator to denominator would lead to divide with zero: "
                    + this);
        }
        return new Lfraction(getDenominator(), getNumerator()); // TODO!!!
    }

    /**
     * Opposite of the fraction. n/d becomes -n/d.
     *
     * @return opposite of this fraction: -this
     */
    public Lfraction opposite() {
        return new Lfraction(-getNumerator(), getDenominator()); // TODO!!!
    }

    /**
     * Difference of fractions.
     *
     * @param m subtrahend
     * @return this-m
     */
    public Lfraction minus(Lfraction m) {
        long denominator = getDenominator() * m.getDenominator();
        long nominator = getNumerator() * m.getDenominator() - m.getNumerator() * getDenominator();
        return new Lfraction(nominator, denominator); // TODO!!!
    }

    /**
     * Quotient of fractions.
     *
     * @param m divisor
     * @return this/m
     */
    public Lfraction divideBy(Lfraction m) {
        if (m.getDenominator() == 0) {
            throw new RuntimeException("Panic! Given fraction has zero in denominator: "
                    + m
                    +". This would lead to dividing with zero.");
        }
        long nominator = getNumerator() * m.getDenominator();
        long denominator = getDenominator() * m.getNumerator();

        return new Lfraction(nominator, denominator); // TODO!!!
    }

    /**
     * Comparision of fractions.
     *
     * @param m second fraction
     * @return -1 if this < m; 0 if this==m; 1 if this > m
     */
    @Override
    public int compareTo(Lfraction m) {
        long cross1 = getNumerator() * m.getDenominator();
        long cross2 = getDenominator() * m.getNumerator();
        if (cross1 < cross2){
            return -1;
        } else if (cross1 > cross2){
            return 1;
        }
        return 0; // TODO!!!
    }

    /**
     * Clone of the fraction.
     *
     * @return new fraction equal to this
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return new Lfraction(getNumerator(), getDenominator()); // TODO!!!
    }

    /**
     * Integer part of the (improper) fraction.
     *
     * @return integer part of this fraction
     */
    public long integerPart() {
        // Help from https://stackoverflow.com/questions/11487731/the-opposite-of-the-modulo-operator
        return getNumerator() / getDenominator(); // TODO!!!
    }

    /**
     * Extract fraction part of the (improper) fraction
     * (a proper fraction without the integer part).
     *
     * @return fraction part of this fraction
     */
    public Lfraction fractionPart() {
        long modulo = getNumerator() % getDenominator();
        return new Lfraction(modulo, getDenominator()); // TODO!!!
    }

    /**
     * Approximate value of the fraction.
     *
     * @return real value of this fraction
     */
    public double toDouble() {
        double numerator = this.numerator.doubleValue();
        double denominator = this.denominator.doubleValue();
        return numerator / denominator; // TODO!!!
    }

    /**
     * Double value f presented as a fraction with denominator d > 0.
     *
     * @param f real number
     * @param d positive denominator for the result
     * @return f as an approximate fraction of form n/d
     */
    public static Lfraction toLfraction(double f, long d) {
        long nominator = Math.round(f * d);
        return new Lfraction(nominator, d); // TODO!!!
    }

    /**
     * Conversion from string to the fraction. Accepts strings of form
     * that is defined by the toString method.
     *
     * @param s string form (as produced by toString) of the fraction
     * @return fraction represented by s
     */
    public static Lfraction valueOf(String s) {
        if (s.isEmpty()){
            throw new RuntimeException("Panic! Can't understand input: " + s);
        }
        // Help from https://stackoverflow.com/questions/18462826/split-string-only-on-first-instance-java
        String[] elements = s.split("/", 2);
        try{
            return new Lfraction(Long.parseLong(elements[0]), Long.parseLong(elements[1]));
        } catch (Exception e){
            throw new RuntimeException("Panic! Got incorrect string: " + s);
        }

    }
}

